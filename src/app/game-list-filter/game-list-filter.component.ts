import {OnInit, Output, EventEmitter, Component } from '@angular/core';
import { GameCategoryFakeApiService } from '../service/game-category-fake-api.service';

export interface GameFilter {
  nameGame: string;
  typeGame: string;
  editorGame: string;
}

@Component({
  selector: 'app-game-list-filter',
  templateUrl: './game-list-filter.component.html',
  styleUrls: ['./game-list-filter.component.css']
})

export class GameListFilterComponent implements OnInit {

  gamesType;

  @Output()
  filter = new EventEmitter<GameFilter>();
  
  constructor() { }

  ngOnInit() {
    new GameCategoryFakeApiService().getAll().subscribe(data => {
      this.gamesType = data;
    });
  }

  form: GameFilter = { nameGame: '', typeGame: '', editorGame: '' };

  onChange(key: string, value: string) {
    if (key !== 'typeGame') { value = value.trim().toLowerCase(); }
    this.form[key] = value;
  }

  onSubmit (event: any) {
    event.preventDefault();
    this.filter.emit(this.form);
  }

  onReset() {
    this.form = {nameGame: '', typeGame: '', editorGame: ''}; 
    this.filter.emit(this.form);
  }
}
