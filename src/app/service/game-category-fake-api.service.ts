import { Injectable } from '@angular/core';
import { CATEGORIES } from './game-fake';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GameCategoryFakeApiService {

  constructor() { }

  getAll() {
    return of(CATEGORIES);
  }
}
