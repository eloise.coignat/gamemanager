import { Injectable } from '@angular/core';
import { GAMES } from './game-fake';
import { Observable, of } from 'rxjs';
import { tap, delay } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { VisitCounterService } from './visit-counter.service';


@Injectable({
  providedIn: 'root'
})
export class GameFakeApiService {

  visitCounter: VisitCounterService

  constructor() { }

  getAll() {
    return of(GAMES)
    .pipe(
      delay(3000)
    );
  }
}
