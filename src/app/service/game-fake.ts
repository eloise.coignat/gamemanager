export const GAMES = [
    {
        id: 0, 
        name:"Mario Kart", 
        type:"Jeu de plateforme", 
        note:4.95, 
        editor:"Nintendo",
        description:"Appuyez sur le champignon et affûtez vos carapaces, Mario Kart 8 Deluxe va tout retourner sur Nintendo Switch ! ",
        url:"https://media.lesechos.com/api/v1/images/view/5db827b08fe56f15991996b8/1280x720/0602147480221-web-tete.jpg" },

    {
        id: 1,
        name:"Pokémon version blanche", 
        type:"MOBA", 
        note:4, 
        editor:"Nintendo1",
        description:"Le joueur entame une quête à travers Unys, capturant les Pokémon sauvages, les entraînant et combattant avec ceux des autres dresseurs Pokémon, avec pour but d'obtenir le titre de « Maître Pokémon »",
        url:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSKh3htSEhLGSyy-8dDnEPUMyi7dMP9fU_rjQuzVIo1Xm337nOyvg&s" },

    {
        id: 2, 
        name:"Sonic VS Mario", 
        type:"RPG", 
        note:3.5, 
        editor:"Nintendo2",
        description:"Comme pour les chiens et les chats (et plus tard les pokémons et les digimons), on se disait : « Soit l’un, soit l’autre. », « Pas de juste milieu. », « Il faut choisir. », ect…",
        url:"https://i.ytimg.com/vi/ekxkIENeaNI/maxresdefault.jpg" }

    ]; 

export const CATEGORIES = [
    'Jeu de plateforme', 
    'RPG', 
    'MOBA', 
    'FPS'
];