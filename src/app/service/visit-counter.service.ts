import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class VisitCounterService {

    constructor() { }

    inc() {
        let visits: number;

        if (localStorage.getItem("visits") != null) {
            visits =+ localStorage.getItem("visits");
        } else {
            visits = 0;
        }
        visits += 1;
        localStorage.setItem('visits', visits.toString());
    }
}