import { Component, OnInit } from '@angular/core';
import { GameList } from './game';
import { GameActions } from './game-actions';
import { GameFakeApiService } from '../service/game-fake-api.service';
import { GameFilter } from '../game-list-filter/game-list-filter.component';
import { VisitCounterService } from '../service/visit-counter.service';

@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.css']
})

export class GameListComponent implements OnInit {

  defaultSize = 400;
  isLoading = false;
  width = this.defaultSize;
  games;
  counter;
  public visits : String;

  constructor(private visitService: VisitCounterService) {}
  
  ngOnInit() {
    new GameFakeApiService().getAll().subscribe(data => {
      this.visitService.inc()
      this.visits = localStorage.getItem("visits");
      this.isLoading = true;
      this.games = data;
    });
    this.counter = localStorage.getItem('visites');
  }

  onActionClick (action: GameActions, game: GameList) {
    // alert('User ' + action + ' ' + game.name)
    alert('User ' + `${['follow', 'share', 'buy', 'remove'][action]}` + ' ' + game.name);
  }

  onShrink () {
    this.width = Math.max(300, this.width - 10);
  }

  onExtend () {
    this.width += 10;
  }

  onResetSize () {
    this.width = this.defaultSize;
  }

  onFilter (filterForm: GameFilter) {
    new GameFakeApiService().getAll().subscribe(data => {
      this.games = data;
    });
    this.games = this.games.filter(e => (!filterForm.nameGame || e.name.toLocaleLowerCase().includes(filterForm.nameGame))
                                  && (!filterForm.typeGame || e.type === filterForm.typeGame)
                                  && (!filterForm.editorGame || e.editor.toLowerCase().includes(filterForm.editorGame)));                                
  }

}


