import { Component, OnInit, Output, EventEmitter} from '@angular/core';
import { GameActions } from './../game-actions';

@Component({
  selector: 'app-game-action',
  templateUrl: './game-action.component.html',
  styleUrls: ['./game-action.component.css']
})
export class GameActionComponent implements OnInit {

  @Output()
  private readonly click = new EventEmitter<GameActions>();

  readonly follow = GameActions.FOLLOW;
  readonly share = GameActions.SHARE;
  readonly buy = GameActions.BUY;
  readonly remove = GameActions.REMOVE;

  constructor() { }

  ngOnInit() {
  }

  // onAction(action: string, event: MouseEvent) {
  //   event.stopPropagation();
  //   event.preventDefault();
  //   this.click.emit(action);
  // }

  onAction(action: GameActions, event: MouseEvent) {
    event.stopPropagation();
    event.preventDefault();
    this.click.emit(action);
  }

}
