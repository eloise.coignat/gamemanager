export class GameList {
    public id?: Number;
    public name: String;
    public type: String;
    public editor: String;
    public note: Number;
    public description: String;
    public url: String;
}