import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GameListComponent } from './game-list/game-list.component';
import { GameListFilterComponent } from './game-list-filter/game-list-filter.component';
import { GameActionComponent } from './game-list/game-action/game-action.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GameFakeApiService } from './service/game-fake-api.service';
import { GameCategoryFakeApiService } from './service/game-category-fake-api.service';
import { VisitCounterService } from './service/visit-counter.service';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    GameListComponent,
    GameListFilterComponent,
    GameActionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatProgressSpinnerModule,
    HttpClientModule
  ],
  providers: [
    GameCategoryFakeApiService,
    GameFakeApiService,
    VisitCounterService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
